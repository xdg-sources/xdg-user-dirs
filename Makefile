PREFIX=/usr
build:
	$(CC) $(CFLAGS) xdg-user-dir.c -o xdg-user-dir
	$(CC) $(CFLAGS) xdg-user-dirs-update.c -o xdg-user-dirs-update
install:
	install xdg-user-dir $(DESTDIR)/$(PREFIX)/bin
	install xdg-user-dirs-update $(DESTDIR)/$(PREFIX)/bin 

clean:
	rm -f xdg-user-dir xdg-user-dirs-update
