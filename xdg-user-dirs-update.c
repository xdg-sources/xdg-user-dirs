#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>

int main(void) {
    char* home = getenv("HOME");
    char* directory = malloc(1024);
    // Downloads directory creation
    strcpy(directory,home);
    strcat(directory,"/Downloads");
    mkdir(directory, S_IRWXU);
    strcpy(directory,home);
    // Desktop directory creation
    strcat(directory,"/Desktop");
    mkdir(directory, S_IRWXU);
    strcpy(directory,home);
    // Config file writing
    strcat(directory,"/.config/user-dirs.dirs");
    FILE *f = fopen(directory,"w");
    fprintf(f,"XDG_DESKTOP_DIR=\"$HOME/Desktop\"\n");
    fprintf(f,"XDG_DOWNLOAD_DIR=\"$HOME/Downloads\"\n");
    fclose(f);
    // Exiting
    free(directory);
    exit(EXIT_SUCCESS);
}
